const path = require('node:path');

module.exports = {
	extension: 'ts',
	ui: 'bdd',
	spec: [
		path.join(__dirname, 'packages', 'common', '**/*.spec.ts'),
		path.join(__dirname, 'packages', 'srv', '**/*.spec.ts'),
		path.join(__dirname, 'packages', 'www', '**/*.spec.ts'),
	],
	require: [
		path.join(__dirname, 'bin', 'mocha-setup.js')
	],
};