// Setup ts-node
require('ts-node').register({
	transpileOnly: true,
	compilerOptions: {
		lib: [ 'es2022' ],
		module: 'commonjs',
		target: 'es2022',
		strict: true,
		esModuleInterop: true,
		skipLibCheck: true,
		forceConsistentCasingInFileNames: true,
		moduleResolution: 'node',
		types: [ 'node' ],
		sourceMap: true,
		inlineSourceMap: false,
		inlineSources: true,
		declaration: false,
		noEmit: false,
		outDir: "./.ts-node"
	}
});

// Setup chai-as-promised
require('chai').use(require('chai-as-promised'));

// Ignore non-standard imports
require('ignore-styles').default([ '.scss', '.tsx' ]);