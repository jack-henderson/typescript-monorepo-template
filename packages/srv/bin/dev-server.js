const { spawn } = require('node:child_process');
const path = require('node:path');


spawn('yarn', [ 'node-dev', path.join('dist', 'main.js') ], {
	stdio: 'inherit',
	cwd: path.dirname(__dirname),
	env: {
		...process.env,
		NODE_ENV: 'development',
	}
});