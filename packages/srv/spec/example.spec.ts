import { COMMON } from '@template/common';
import { expectHttp, useTestServer } from './helpers';


describe('[srv] example', () => {
	const { server } = useTestServer();

	it('passes', async () => {
		const response = await server().inject('/api');
		expectHttp(response, 200, { hello: COMMON });
	});
});