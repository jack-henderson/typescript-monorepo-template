import type { FastifyInstance, LightMyRequestResponse } from 'fastify';
import { assert, expect } from 'chai';
import { pino } from 'pino';
import { makeServer } from '../src/server/server';


export type TestServer = {
	server: () => FastifyInstance;
};
/**
 * Creates a fresh server instance for each test.
 */
export const useTestServer = (): TestServer => {
	let server: FastifyInstance;

	beforeEach(() => {
		server = makeServer({
			logger: pino({ level: 'silent' }),
		});
	});
	afterEach(async () => {
		await server?.close();
	});

	return {
		server: () => server,
	};
};

export const expectHttp = (response: LightMyRequestResponse, statusCode: number, body?: Record<string, unknown>): void => {
	if(response.statusCode !== statusCode) {
		const message = `Expected status code ${statusCode}, but got ${response.statusCode} with body ${JSON.stringify(response.json(), null, 2)}`;
		assert(false,  message);
	}

	if(body) {
		expect(response.json())
			.to.deep.eq(body);
	}
};
