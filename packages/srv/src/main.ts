import 'source-map-support/register';

import closeWithGrace from 'close-with-grace';
import { pino } from 'pino';
import { makeServer } from './server/server';


const logger = pino();
const server = makeServer({
	logger: logger.child({ name: 'server' }),
});

server.listen({ host: '0.0.0.0', port: 8080 })
	.then(() => {
		if(process.env['NODE_ENV'] === 'development') {
			console.log('🚀 Server started on http://localhost:8080'); // eslint-disable-line no-console
		}
	});

closeWithGrace(async ({ err, signal }) => {
	if(err) {
		logger.error(err, 'Unhandled error');
	} else {
		logger.info('Received signal %s', signal);
	}

	await server.close();
});