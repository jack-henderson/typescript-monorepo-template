import type { FastifyInstance } from 'fastify';
import type { Logger } from 'pino';
import { COMMON } from '@template/common';
import { fastify } from 'fastify';


export type ServerDependencies = {
	logger: Logger;
};

export const makeServer = (deps: ServerDependencies): FastifyInstance => (
	fastify({
		logger: deps.logger,
	}).get('/api', () => ({ hello: COMMON }))
);