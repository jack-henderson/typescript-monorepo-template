import type { FastifyPluginAsync, FastifyPluginOptions } from 'fastify';


const skipOverride = Symbol.for('skip-override');

/**
 * Avoid the creation of a new Fastify context for the passed `plugin`.
 *
 * https://www.fastify.io/docs/latest/Reference/Plugins/#handle-the-scope
 */
export const makeRootPlugin = <Options extends FastifyPluginOptions>(plugin: FastifyPluginAsync<Options>): FastifyPluginAsync<Options> => {
	(plugin as any)[skipOverride] = true; // eslint-disable-line @typescript-eslint/no-explicit-any
	return plugin;
};
