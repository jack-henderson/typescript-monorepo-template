import type { JSX } from 'preact/jsx-runtime';
import { useEffect, useState } from 'preact/hooks';
import { COMMON } from '@template/common';
import * as css from './example.module.scss';


export const Example = (): JSX.Element => {
	const [ data, setData ] = useState('loading...');

	useEffect(() => {
		let abort: AbortController | null = new AbortController();

		fetch('/api', { signal: abort.signal })
			.then(r => {
				abort = null;
				return r.json();
			})
			.then(j => setData(JSON.stringify(j, null, 2)))
			// eslint-disable-next-line no-console
			.catch(err => console.error('Failed to fetch from API:', err));

		return () => {
			if(abort) {
				abort.abort();
			}
		};
	}, []);

	return (
		<div className={ css.example }>
			<p>This is an example component which fetches from the API.</p>
			<pre>
				{ data }
			</pre>
			<p>Imported from common: "{ COMMON }"</p>
		</div>
	);
};