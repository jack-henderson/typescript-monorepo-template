import type { JSX } from 'preact';
import { render } from 'preact';
import { Example } from './components';
import './theme/global.scss';


const Root = (): JSX.Element => {

	return (
		<Example/>
	);
};

window.addEventListener('load', () => {
	const root = document.getElementById('root');
	if(!root) {
		throw new Error('Missing #root.');
	}

	render(<Root/>, root);
});